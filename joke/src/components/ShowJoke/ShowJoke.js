import React, {Component} from 'react';

class ShowJoke extends Component {
    render () {
        return (
            <div>
                <p>{this.props.joke}</p>
            </div>
        );
    }
}

export default ShowJoke;