import React, {Component} from 'react';

class NewJoke extends Component{
    render () {
        return (
            <div>
                <button className="btn" onClick={this.props.getJokeHandler}>Get new joke</button>
            </div>
        );
    }
}

export default NewJoke;