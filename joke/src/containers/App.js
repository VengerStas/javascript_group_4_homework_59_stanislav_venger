import React, { Component } from 'react';
import './App.css';
import NewJoke from "../components/NewJoke/NewJoke";
import ShowJoke from "../components/ShowJoke/ShowJoke";

class App extends Component {
    state = {
        joke: []
    };

  getJoke = () => {
      const P_URL = "https://api.chucknorris.io/jokes/random";
      fetch(P_URL).then(response => {
          if (response.ok) {
              return response.json();
          }
          throw new Error('Something went wrong with request');
      }).then(joke => {
          const jokeUpdate = {joke: joke.value};
          this.setState({joke: jokeUpdate})
      }).catch(error => {
          console.log(error);
      })
  };

  componentDidMount() {
      this.getJoke();
  }

  render() {
    return (
      <div className="App">
          <NewJoke
            getJokeHandler = {() => this.getJoke()}
          />
          <ShowJoke
              joke={this.state.joke.joke}
          />
      </div>
    );
  }
}

export default App;
