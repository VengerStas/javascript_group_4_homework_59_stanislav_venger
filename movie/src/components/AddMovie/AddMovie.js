import React from 'react';
import './AddMovie.css';

const AddMovie = props => {
    return (
        <div className="add-movie">
            <input type="text" value={props.value} onChange={props.changeHandler} className="input-movie"/>
            <button className="add" onClick={props.addHandler}>Add</button>
        </div>
    );
};

export default AddMovie;