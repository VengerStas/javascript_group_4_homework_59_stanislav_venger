import React, {Component} from 'react';
import './MoviesBuilder.css';
class MoviesBuilder extends Component{

    shouldComponentUpdate (nextProps) {
        return (
            nextProps.name !== this.props.name
        )
    }

    render () {
        return (
            <div className="list">
                <input className="movie-edit" type="text" value={this.props.name} onChange={(event) => this.props.newMovieChange(event, this.props.index)}/>
                <button onClick={this.props.removeHandler} className="delete">Delete</button>
            </div>
        );
    }
}

export default MoviesBuilder;