import React, { Component } from 'react';
import './App.css';
import MoviesBuilder from "../components/MoviesBuilder/MoviesBuilder";
import AddMovie from "../components/AddMovie/AddMovie";

class App extends Component {
  state = {
      newMovie: '',
      movie: [],
      currentMovie: 0
  };

  changeHandler = event => {
      this.setState({newMovie: event.target.value})
  };

  addMovie = () => {
      if (this.state.newMovie !== ''){
          const movie = [...this.state.movie];
          const newMovie = {name: this.state.newMovie, id: this.state.currentMovie + 1};
          movie.push(newMovie);

          this.setState({movie, currentMovie: this.state.currentMovie + 1, newMovie: ''});
      }
  };

  deleteMovie = id => {
        const movie = [...this.state.movie];
        const index = movie.findIndex(movie => {
            return (
                movie.id === id
            )
        });
        movie.splice(index, 1);

        this.setState({movie, currentMovie: this.state.currentMovie - 1})
  };

  newMovieChanger = (event, index) => {
      let movie = this.state.movie;
      movie[index].name = event.target.value;
      this.setState({movie})
  };



  render() {
      let list = this.state.movie.map((movie, id) => {
          return (
              <div className="movie-box" key = {id}>
                  <MoviesBuilder
                      name = {movie.name}
                      removeHandler = {() => this.deleteMovie(movie.id)}
                      newMovieChange = {this.newMovieChanger}
                      index = {id}
                  />
              </div>
          )
      });
    return (
      <div className="App">
          <AddMovie
            addHandler = {() => this.addMovie()}
            changeHandler = {event => this.changeHandler(event)}
            value = {this.state.newMovie}
          />
          <p>To watch list:</p>
          {list}
      </div>
    );
  }
}

export default App;
